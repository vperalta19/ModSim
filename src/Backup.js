import "./App.css";
import "../node_modules/react-vis/dist/style.css";
import React, { useState } from "react";
import {
  XAxis,
  YAxis,
  HorizontalGridLines,
  LineMarkSeries,
  VerticalGridLines,
  Hint,
  FlexibleXYPlot,
  Highlight,
} from "react-vis";
import * as math from "mathjs";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import { makeStyles } from "@material-ui/core/styles";
import Slider from "@material-ui/core/Slider";
import RemoveIcon from "@material-ui/icons/Remove";
import Typography from "@material-ui/core/Typography";
import { green, red, orange } from "@material-ui/core/colors";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";

function BackUp() {
  const classes = useStyles();
  const [funcion, setFuncion] = useState("x+y");
  const [x0, setX0] = useState(1);
  const [t0, setT0] = useState(0);
  const [tf, setTF] = useState(2);
  const [n, setN] = useState(5);
  const [lastDrawLocation, setLastDrawLocation] = useState(null);
  const [fullWidth, setFullWidth] = React.useState(true);
  const [maxWidth, setMaxWidth] = React.useState('sm');

  const funcionEuler = (funcion, xo, xf, yo, n) => {
    var h = (xf - xo) / n;
    var aux = [];
    var x = xo;
    var y = yo;
    aux.push({ x: x, y: y });
    while (x < xf) {
      const parser = math.parser();
      parser.evaluate("f(x,y)=" + funcion);
      y = y + h * parser.evaluate("f(" + x + "," + y + ")");
      x = x + h;
      aux.push({ x: x, y: y });
    }
    return aux;
  };

  const funcionEulerMejorado = (funcion, xo, xf, yo, n) => {
    var h = (xf - xo) / n;
    var aux = [];
    var x = xo;
    var xAux = 0;
    var y = yo;
    aux.push({ x: x, y: y });
    const parser2 = math.parser();
    var yAnterior = 0;
    while (x < xf) {
      parser2.evaluate("f(x,y)=" + funcion);
      yAnterior = y;
      y = y + h * parser2.evaluate("f(" + x + "," + y + ")");
      xAux = x;
      x = x + h;
      y =
        yAnterior +
        0.5 *
          h *
          (parser2.evaluate("f(" + xAux + "," + yAnterior + ")") +
            parser2.evaluate("f(" + x + "," + y + ")"));
      aux.push({ x: x, y: y });
    }
    return aux;
  };
  const funcionRungeKutta = (funcion, xo, xf, yo, n) => {
    var h = (xf - xo) / n;
    var aux = [];
    var x = xo;
    var xAnterior = 0;
    var yAnterior = 0;
    var p = 0;
    var q = 0;
    var r = 0;
    var s = 0;
    var y = yo;
    aux.push({ x: x, y: y });
    const parser2 = math.parser();
    while (x < xf) {
      parser2.evaluate("f(x,y)=" + funcion);
      yAnterior = y;
      xAnterior = x;
      x = xAnterior + h;
      p = parser2.evaluate("f(" + xAnterior + "," + yAnterior + ")") * h;
      var qAux = xAnterior + 0.5 * h;
      var qAux2 = yAnterior + 0.5 * p;
      q = parser2.evaluate("f(" + qAux + "," + qAux2 + ")") * h;
      var rAux = yAnterior + 0.5 * q;
      r = parser2.evaluate("f(" + qAux + "," + rAux + ")") * h;
      var sAux = xAnterior + h;
      var sAux2 = yAnterior + r;
      s = parser2.evaluate("f(" + sAux + "," + sAux2 + ")") * h;
      y = yAnterior + (1 / 6) * (p + 2 * q + 2 * r + s);

      aux.push({ x: x, y: y });
    }
    return aux;
  };

  function valuetext(value) {
    return `${value}`;
  }
  const onClick = () => {
    setFuncion(document.getElementById("text-funcion").value);
    setX0(parseFloat(document.getElementById("text-x0").value));
    setT0(parseFloat(document.getElementById("text-t0").value));
    setTF(parseFloat(document.getElementById("text-tf").value));
    setN(parseFloat(document.getElementById("text-n").value));
  };

  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <Typography
        style={{
          fontSize: 25,
          backgroundColor: "#F9D576",
          padding: 10,
          color: orange[900],
        }}
        align="center"
      >
        EULER / EULER MEJORADO / RUNGE - KUTTE
      </Typography>
      <Grid container style={{ backgroundColor: "#FEEBB9" }}>
        <Grid
          style={{
            width: 900,
            display: "flex",
            paddingLeft: 20,
            marginTop: 20,
            marginBottom: 33,
          }}
          item
          xs={8}
        >
          {" "}
          <FlexibleXYPlot
            width={900}
            height={650}
            style={{ backgroundColor: "#FFFFFF" }}
            dontCheckIfEmpty={true}
            xDomain={
              lastDrawLocation && [
                lastDrawLocation.left,
                lastDrawLocation.right,
              ]
            }
            yDomain={
              lastDrawLocation && [
                lastDrawLocation.bottom,
                lastDrawLocation.top,
              ]
            }
          >
            <VerticalGridLines />
            <HorizontalGridLines />
            <XAxis />
            <YAxis />
            <LineMarkSeries
              dontCheckIfEmpty={true}
              className="linemark-series-example"
              style={{
                strokeWidth: "3px",
              }}
              lineStyle={{ stroke: "red" }}
              markStyle={{ stroke: "blue" }}
              data={funcionEuler(funcion, t0, tf, x0, n)}
              animation={true}
              size={2}
            />
            <LineMarkSeries
              className="linemark-series-example"
              style={{
                strokeWidth: "3px",
              }}
              lineStyle={{ stroke: "green" }}
              markStyle={{ stroke: "blue" }}
              data={funcionEulerMejorado(funcion, t0, tf, x0, n)}
              animation={true}
              size={2}
            />
            <LineMarkSeries
              className="linemark-series-example"
              style={{
                strokeWidth: "3px",
              }}
              lineStyle={{ stroke: "black" }}
              markStyle={{ stroke: "blue" }}
              data={funcionRungeKutta(funcion, t0, tf, x0, n)}
              animation={true}
              size={2}
            />
            <Highlight
              onBrushEnd={(area) => {
                setLastDrawLocation(area);
              }}
            />
          </FlexibleXYPlot>
        </Grid>
        <Grid
          className={classes.paper}
          style={{ paddingRight: 30 }}
          item
          xs={4}
        >
          <Button
            variant="outlined"
            color="secondary"
            onClick={handleClickOpen}
            className={classes.button2}
          >
            glosario
          </Button>
          <Dialog
            fullWidth={fullWidth}
            maxWidth={maxWidth}
            open={open}
            onClose={handleClose}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
          >
            <DialogTitle id="alert-dialog-title">{"Glosario"}</DialogTitle>
            <DialogContent>
              <DialogContentText id="alert-dialog-description">
              <Typography>
                Lista de sintaxis de las operaciones matemáticas a utilizar:
              </Typography>
              <Grid container>
                <Grid style={{
                  paddingLeft: 20,
                  marginTop: 20,
                  marginBottom: 15,
                  marginLeft: 50,
                  marginRight: 50,
                }}>
                  <Typography>
                    • Suma: +
                  </Typography>
                  <Typography>
                  • Resta: -
                  </Typography>
                  <Typography>
                  • Multiplicación: *
                  </Typography>
                  <Typography>
                  • División: /
                  </Typography>
                  <Typography>
                  • Raíz cuadrada: sqrt()
                  </Typography>
                  <Typography>
                  • Raíz cúbica: cbrt()
                  </Typography>
                  <Typography>
                  • Potencia: ^
                  </Typography>
                  <Typography>
                  • Seno: sin()
                  </Typography>
                  <Typography>
                  • Arco seno: asin()
                  </Typography>
                  <Typography>
                  • Coseno: cos()
                  </Typography>
                  
                </Grid>
                <Grid style={{
                  paddingLeft: 20,
                  marginTop: 20,
                }}>
                  <Typography>
                  • Arco coseno: acos()
                  </Typography>
                  <Typography>
                  • Tangente: tan()
                  </Typography>
                  <Typography>
                  • Arco tangente: atan()
                  </Typography>
                  <Typography>
                  • Cotangente: cot()
                  </Typography>
                  <Typography>
                  • Arco cotangente: acot()
                  </Typography>
                  <Typography>
                  • Secante: sec()
                  </Typography>
                  <Typography>
                  • Arco secante: asec()
                  </Typography>
                  <Typography>
                  • Cosecante: csc()
                  </Typography>
                  <Typography>
                  • Arco cosecante: acsc()
                  </Typography>
                  <Typography>
                  • Función exponencial: e^ 
                  </Typography>
                    
                </Grid>
                <Typography style={{
                  marginLeft: 50,
                  marginRight: 30,
                  marginBottom: 15,
                }}>
                  • Funciones seno, coseno, tangente, cotangente, secante y cosecante hiperbólicas:
                  se escriben igual que las definiciones originales, con una "h" antes de abrir los
                  paréntesis.
                  </Typography>
                  <Typography style={{
                  marginLeft: 50,
                  marginRight: 30,
                }}>
                  • Funciones arco seno, coseno, tangente, cotangente, secante y cosecante
                  hiperbólicas: se escriben igual que las definiciones originales, con una "a" al
                  principio.
                  </Typography>
              </Grid>
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button onClick={handleClose} className={classes.aceptar} autoFocus>
                Aceptar
              </Button>
            </DialogActions>
          </Dialog>
          <TextField
            className={classes.textFieldTop}
            id="text-funcion"
            style={{ width: 250 }}
            variant="outlined"
            margin="dense"
            required
            fullWidth
            label="Función"
            defaultValue="x+y"
          />

          <TextField
            className={classes.textField}
            id="text-x0"
            style={{ width: 250 }}
            variant="outlined"
            margin="dense"
            required
            fullWidth
            label="Y(0)"
            defaultValue="1"
          />
          <TextField
            className={classes.textField}
            id="text-t0"
            style={{ width: 250 }}
            variant="outlined"
            margin="dense"
            required
            fullWidth
            label="Xi"
            defaultValue="0"
          />
          <TextField
            className={classes.textField}
            id="text-tf"
            style={{ width: 250 }}
            variant="outlined"
            margin="dense"
            required
            fullWidth
            label="Xf"
            defaultValue="2"
          />
          <TextField
            className={classes.textField}
            value={n}
            id="text-n"
            style={{ width: 250 }}
            variant="outlined"
            margin="dense"
            required
            fullWidth
            label="N"
            defaultValue="5"
          />
          <Slider
            defaultValue={5}
            id="text-nSlider"
            value={parseInt(n)}
            getAriaValueText={valuetext}
            aria-labelledby="discrete-slider-small-steps"
            step={1}
            marks
            min={1}
            max={20}
            valueLabelDisplay="auto"
            style={{ width: 250, marginTop: 20, color: "firebrick" }}
            onChange={(e, val) => setN(val)}
          />

          <Button
            className={classes.button}
            variant="contained"
            color="secondary"
            onClick={onClick}
          >
            GRAFICAR
          </Button>
          <Grid container>
            <Grid>
              <RemoveIcon
                style={{
                  fontSize: 50,
                  color: red[500],
                }}
              />
            </Grid>
            <Grid>
              <Typography
                style={{ fontSize: 20, marginTop: 10, color: red[500] }}
              >
                {" "}
                Euler{" "}
              </Typography>
            </Grid>
          </Grid>
          <Grid container>
            <Grid>
              <RemoveIcon style={{ fontSize: 50, color: green[700] }} />
            </Grid>
            <Grid>
              <Typography
                style={{ fontSize: 20, marginTop: 10, color: green[700] }}
              >
                {" "}
                Euler Mejorado{" "}
              </Typography>
            </Grid>
          </Grid>
          <Grid container>
            <Grid>
              <RemoveIcon style={{ fontSize: 50 }} />
            </Grid>
            <Grid>
              <Typography style={{ fontSize: 20, marginTop: 10 }}>
                {" "}
                R-K{" "}
              </Typography>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </div>
  );
}

export default BackUp;

const useStyles = makeStyles((theme) => ({
  paper: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    boxShadow: "none",
    backgroundColor: "transparent",
  },

  textField: {
    background: "white",
    width: 250,
    marginTop: 25,
  },

  textFieldTop: {
    background: "white",
    width: 250,
    marginTop: 35,
  },

  button: {
    marginTop: 20,
    color: "navajowhite",
    backgroundColor: "firebrick",
  },

  button2: {
    marginTop: 25,
    width: 250,
    marginBottom: 5,
    color: orange[900],
    borderColor: orange[900],
  },

  aceptar: {
    color: orange[900],
  },

  fondo: {
    color: "#FEECBE",
  },
}));
