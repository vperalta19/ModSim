import "./App.css";
import "../node_modules/react-vis/dist/style.css";
import React, { useState } from "react";
import {
  XAxis,
  YAxis,
  HorizontalGridLines,
  VerticalGridLines,
  LineSeries,
  MarkSeries,
  FlexibleXYPlot,
} from "react-vis";
import * as math from "mathjs";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import { orange } from "@material-ui/core/colors";
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Box from '@material-ui/core/Box';

function Montecarlo() {
  const [funcion, setFuncion] = useState("3x^2");
  const [a, setA] = useState(0);
  const [b, setB] = useState(1);
  const [n, setN] = useState(1000);
  const [fullWidth, setFullWidth] = React.useState(true);
  const [maxWidth, setMaxWidth] = React.useState('sm');
  const funcionMontecarlo = (funcion, a, b, n) => {
    var arrayPuntos = [];
    const parser = math.parser();
    parser.evaluate("f(x)=" + funcion);
    var xRandom = 0;
    var yRandom = 0;
    var yReal = 0;
    var puntosDisparados = 0;
    var puntosAcertados = 0;
    var h = (b - a) / n;
    var maximo = parser.evaluate("f(" + a + ")");
    var minimo = parser.evaluate("f(" + a + ")");
    for (var i = a + h; i <= b; i = i + h) {
      var aux = parser.evaluate("f(" + i + ")");
      if (maximo < aux) {
        maximo = aux;
      }
      if (aux < minimo) {
        minimo = aux;
      }
    }
    var alturaMaximaRectangulo = maximo;
    var alturaMinimaRectangulo = minimo;
    while (puntosDisparados < n) {
      xRandom = Math.random() * (b - a) + a;
      yRandom =
        Math.random() * (alturaMaximaRectangulo - alturaMinimaRectangulo) +
        alturaMinimaRectangulo;
      yReal = parser.evaluate("f(" + xRandom + ")");
      if (yRandom >= 0) {
        if (yReal >= yRandom) {
          puntosAcertados++;
          arrayPuntos.push({ x: xRandom, y: yRandom, color: "green", size: 1 });
        } else {
          arrayPuntos.push({ x: xRandom, y: yRandom, color: "red", size: 1 });
        }
      } else {
        if (yReal > yRandom) {
          arrayPuntos.push({ x: xRandom, y: yRandom, color: "red", size: 1 });
        } else {
          puntosAcertados--;
          arrayPuntos.push({ x: xRandom, y: yRandom, color: "green", size: 1 });
        }
      }
      puntosDisparados++;
    }
    console.log("Aciertos: " + puntosAcertados);
    console.log("Disparos: " + puntosDisparados);
    console.log("b: " + b);
    console.log("a: " + a);
    console.log("Maximo: " + alturaMaximaRectangulo);
    console.log("Minimo: " + alturaMinimaRectangulo);

    var integral =
      (puntosAcertados / puntosDisparados) *
      (b - a) *
      (alturaMaximaRectangulo - alturaMinimaRectangulo);
    console.log(integral);
    localStorage.setItem("resultado", integral);
    return [arrayPuntos];
  };
  const graficarFuncion = (funcion, a, b) => {
    console.log(funcion);
    var arrayAux = [];
    const h = 0.01;
    const parser = math.parser();
    parser.evaluate("f(x)=" + funcion);
    for (var i = a; i <= b; i = i + h) {
      var aux = parser.evaluate("f(" + i + ")");
      arrayAux.push({ x: i, y: aux });
    }

    return arrayAux;
  };
  const classes = useStyles();
  const onClick = () => {
    setFuncion(document.getElementById("tf-funcion").value);
    setA(parseFloat(document.getElementById("tf-a").value));
    setB(parseFloat(document.getElementById("tf-b").value));
    setN(parseFloat(document.getElementById("tf-n").value));
  };

  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };


  return (
    <div>
      <Typography
        style={{
          fontSize: 25,
          backgroundColor: "#F9D576",
          padding: 10,
          color: orange[900],
        }}
        align="center"
      >
        MONTECARLO
      </Typography>
      <Grid container style={{ backgroundColor: "#FEEBB9" }}>
        <Grid className={classes.paper} item xs={4}>
          <Button variant="outlined" color="secondary" onClick={handleClickOpen} className={classes.button2}>
            glosario
          </Button>
          <Dialog
            open={open}
            onClose={handleClose}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
          >
            <DialogTitle id="alert-dialog-title">{"Glosario"}</DialogTitle>
            <DialogContent>
            <DialogContentText id="alert-dialog-description">
              <Typography>
                Lista de sintaxis de las operaciones matemáticas a utilizar:
              </Typography>
              <Grid container>
                <Grid style={{
                  paddingLeft: 20,
                  marginTop: 20,
                  marginBottom: 15,
                  marginLeft: 50,
                  marginRight: 50,
                }}>
                  <Typography>
                    • Suma: +
                  </Typography>
                  <Typography>
                  • Resta: -
                  </Typography>
                  <Typography>
                  • Multiplicación: *
                  </Typography>
                  <Typography>
                  • División: /
                  </Typography>
                  <Typography>
                  • Raíz cuadrada: sqrt()
                  </Typography>
                  <Typography>
                  • Raíz cúbica: cbrt()
                  </Typography>
                  <Typography>
                  • Potencia: ^
                  </Typography>
                  <Typography>
                  • Seno: sin()
                  </Typography>
                  <Typography>
                  • Arco seno: asin()
                  </Typography>
                  <Typography>
                  • Coseno: cos()
                  </Typography>
                  
                </Grid>
                <Grid style={{
                  paddingLeft: 20,
                  marginTop: 20,
                }}>
                  <Typography>
                  • Arco coseno: acos()
                  </Typography>
                  <Typography>
                  • Tangente: tan()
                  </Typography>
                  <Typography>
                  • Arco tangente: atan()
                  </Typography>
                  <Typography>
                  • Cotangente: cot()
                  </Typography>
                  <Typography>
                  • Arco cotangente: acot()
                  </Typography>
                  <Typography>
                  • Secante: sec()
                  </Typography>
                  <Typography>
                  • Arco secante: asec()
                  </Typography>
                  <Typography>
                  • Cosecante: csc()
                  </Typography>
                  <Typography>
                  • Arco cosecante: acsc()
                  </Typography>
                  <Typography>
                  • Función exponencial: e^ 
                  </Typography>
                    
                </Grid>
                <Typography style={{
                  marginLeft: 50,
                  marginRight: 30,
                  marginBottom: 15,
                }}>
                  • Funciones seno, coseno, tangente, cotangente, secante y cosecante hiperbólicas:
                  se escriben igual que las definiciones originales, con una "h" antes de abrir los
                  paréntesis.
                  </Typography>
                  <Typography style={{
                  marginLeft: 50,
                  marginRight: 30,
                }}>
                  • Funciones arco seno, coseno, tangente, cotangente, secante y cosecante
                  hiperbólicas: se escriben igual que las definiciones originales, con una "a" al
                  principio.
                  </Typography>
              </Grid>
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button onClick={handleClose} className={classes.aceptar} autoFocus>
                Aceptar
              </Button>
            </DialogActions>
          </Dialog>
            <TextField
              id="tf-funcion"
              className={classes.textFieldTop}
              style={{ width: 250 }}
              variant="outlined"
              margin="dense"
              required
              fullWidth
              label="Función"
              defaultValue="3x^2"
            />

            <TextField
              id="tf-a"
              className={classes.textField}
              style={{ width: 250 }}
              variant="outlined"
              margin="dense"
              required
              fullWidth
              label="a"
              defaultValue="0"
            />
            <TextField
              id="tf-b"
              className={classes.textField}
              style={{ width: 250 }}
              variant="outlined"
              margin="dense"
              required
              fullWidth
              label="b"
              defaultValue="1"
            />

            <TextField
              id="tf-n"
              className={classes.textField}
              style={{ width: 250 }}
              variant="outlined"
              margin="dense"
              required
              fullWidth
              label="N"
              defaultValue="1000"
            />

            <Button
              className={classes.button}
              variant="contained"
              color="secondary"
              onClick={onClick}
            >
              GRAFICAR
            </Button>

            {localStorage.getItem("resultado") !== null && (
              <Typography className={classes.textFieldResultado} align="center">
                El resultado de Montecarlo es: <Box fontWeight="fontWeightBold">{localStorage.getItem("resultado")}</Box>
              </Typography>
            )}

          </Grid>
          <Grid className={classes.paper} 
          style={{
                marginBottom: 25,
              }}
              item xs={8}>
            <FlexibleXYPlot
              width={900}
              height={650}
              style={{
                backgroundColor: "#FFFFFF",
                position: "center",
                marginTop: 20,
              }}
              dontCheckIfEmpty={true}
            >
              <VerticalGridLines />
              <HorizontalGridLines />
              <XAxis />
              <YAxis />
              <LineSeries
                className="linemark-series-example"
                style={{
                  strokeWidth: "2px",
                  color: "black",
                }}
                lineStyle={{ stroke: "black" }}
                data={graficarFuncion(funcion, a, b)}
                size={2}
              />
              <MarkSeries
                className="mark-series-example"
                sizeRange={[1, 3]}
                strokeWidth={2}
                data={funcionMontecarlo(funcion, a, b, n)[0]}
                colorType="literal"
              />
            </FlexibleXYPlot>
          </Grid>
        </Grid>
    </div>
  );
}

export default Montecarlo;

const useStyles = makeStyles((theme) => ({
  paper: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    boxShadow: "none",
    backgroundColor: "transparent",
  },

  textField: {
    background: "white",
    width: 250,
    marginTop: 25,
  },

  textFieldResultado: {
    width: 250,
    marginTop: 50,
    fontSize: 18,
    color: orange[900],
  },

  textFieldTop: {
    background: "white",
    width: 250,
    marginTop: 35,
  },

  button: {
    marginTop: 20,
    color:"navajowhite",
    backgroundColor:"firebrick",
  },

  button2: {
    marginTop: 25,
    width: 250,
    marginBottom: 5,
    color: orange[900],
    borderColor: orange[900],
  },

  aceptar: {
    color: orange[900],
  },

  fondo: {
    color: "#FEECBE",
  },
}));
