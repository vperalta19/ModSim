import logo from "./logo.svg";
import "./App.css";
import "../node_modules/react-vis/dist/style.css";
import {
  XYPlot,
  XAxis,
  YAxis,
  HorizontalGridLines,
  LineSeries,
  LineMarkSeries,
  VerticalGridLines,
  LineSeriesCanvas,
} from "react-vis";

function App() {
  const funcionEuler = (funcion, to, tf, xo, n) => {
    funcion = reemplazoSignos(funcion);
    var h = (tf - to) / n; // ver el tema si es positivo o negativo
    var aux = [];
    var x = to;
    var y = xo;
    // console.log(h);
    aux.push({ x: x, y: y });
    while (x < tf) {
      console.log(funcion);
      y = y + h * eval(funcion);
      x = x + h;
      aux.push({ x: x, y: y });
    }
    console.log(aux);
    return aux;
  };
  const reemplazoSignos = (funcion) => {
    if (funcion.toString().includes("e")) {
      funcion = funcion.toString().replace("e", Math.exp(1).toString());
    }
    /*  if (funcion.toString().includes("^")) {
      var i = 1;
      var index = funcion.indexOf("^");
      var base = funcion.substring(index - 1, index);
      var repeticion = funcion.substring(index + 1, index + 2);
      var aux = "";
      while (i < repeticion) {
        aux = aux + base + "*";
        i++;
      }
      aux = aux + base;
      var funcionNueva =
        funcion.substring(0, index - 1) + aux + funcion.substring(index + 2);
      console.log(funcionNueva);
    } */

    // console.log(auxx);
    //  console.log(funcion);
    //   console.log(funcion);
    return funcion;
  };

  return (
    <XYPlot width={800} height={800}>
      <VerticalGridLines />
      <HorizontalGridLines />
      <XAxis />
      <YAxis />
      <LineMarkSeries
        className="linemark-series-example"
        style={{
          strokeWidth: "3px",
        }}
        lineStyle={{ stroke: "red" }}
        markStyle={{ stroke: "blue" }}
        data={funcionEuler("-e^-x", 0, 1, 1, 5)}
      />
    </XYPlot>
  );
}

export default App;
