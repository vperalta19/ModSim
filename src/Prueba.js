import "./App.css";
import "../node_modules/react-vis/dist/style.css";
import React, { useState, useEffect } from "react";
import {
  XYPlot,
  XAxis,
  YAxis,
  HorizontalGridLines,
  LineMarkSeries,
  VerticalGridLines,
  Hint,
  LineSeries,
  FlexibleXYPlot,
} from "react-vis";
import * as math from "mathjs";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import { makeStyles } from "@material-ui/core/styles";
import Slider from "@material-ui/core/Slider";
import RemoveIcon from "@material-ui/icons/Remove";
import Typography from "@material-ui/core/Typography";
import { green, red, orange } from "@material-ui/core/colors";
import ButtonGroup from "@material-ui/core/ButtonGroup";

function Prueba() {
  const classes = useStyles();
  const [valor, setValor] = useState(false);
  const [funcion, setFuncion] = useState("");
  const [x0, setX0] = useState(0);
  const [t0, setT0] = useState(0);
  const [tf, setTF] = useState(0);
  const [n, setN] = useState(0);
  const [cambiandoProps, setCambiandoProps] = useState(false);
  const [graficarFuncion, setGraficarFuncion] = useState(false);
  const funcionEuler = (funcion, xo, xf, yo, n) => {
    var h = (xf - xo) / n; // ver el tema si es positivo o negativo
    var aux = [];
    var x = xo;
    var y = yo;
    aux.push({ x: x, y: y });

    while (x < xf) {
      const parser = math.parser();
      parser.evaluate("f(x,y)=" + funcion);
      y = y + h * parser.evaluate("f(" + x + "," + y + ")");
      x = x + h;
      aux.push({ x: x, y: y });
    }
    console.log(aux);
    return aux;
  };

  const funcionEulerMejorado = (funcion, xo, xf, yo, n) => {
    var h = (xf - xo) / n; // ver el tema si es positivo o negativo
    var aux = [];
    var x = xo;
    var xAux = 0;
    var y = yo;
    aux.push({ x: x, y: y });
    const parser2 = math.parser();
    var yAnterior = 0;
    while (x < xf) {
      parser2.evaluate("f(x,y)=" + funcion);
      yAnterior = y;
      y = y + h * parser2.evaluate("f(" + x + "," + y + ")");
      xAux = x;
      x = x + h;
      y =
        yAnterior +
        0.5 *
          h *
          (parser2.evaluate("f(" + xAux + "," + yAnterior + ")") +
            parser2.evaluate("f(" + x + "," + y + ")"));
      aux.push({ x: x, y: y });
      // setPuntosAnteriores3(puntosAnteriores3.concat({ x: x, y: y }));
      //  setPuntosAnteriores3((searches) => [...searches, { x: x, y: y }]);
    }

    console.log(aux);
    return aux;
  };
  const funcionRungeKutta = (funcion, xo, xf, yo, n) => {
    var h = (xf - xo) / n; // ver el tema si es positivo o negativo
    var aux = [];
    var x = xo;
    var xAnterior = 0;
    var yAnterior = 0;
    var p = 0;
    var q = 0;
    var r = 0;
    var s = 0;
    var y = yo;
    aux.push({ x: x, y: y });
    const parser2 = math.parser();
    while (x < xf) {
      parser2.evaluate("f(x,y)=" + funcion);
      yAnterior = y;
      xAnterior = x;
      x = xAnterior + h;
      p = parser2.evaluate("f(" + xAnterior + "," + yAnterior + ")") * h;
      var qAux = xAnterior + 0.5 * h;
      var qAux2 = yAnterior + 0.5 * p;
      q = parser2.evaluate("f(" + qAux + "," + qAux2 + ")") * h;
      var rAux = yAnterior + 0.5 * q;
      r = parser2.evaluate("f(" + qAux + "," + rAux + ")") * h;
      var sAux = xAnterior + h;
      var sAux2 = yAnterior + r;
      s = parser2.evaluate("f(" + sAux + "," + sAux2 + ")") * h;
      y = yAnterior + (1 / 6) * (p + 2 * q + 2 * r + s);
      aux.push({ x: x, y: y });
    }
    console.log(aux);
    return aux;
  };

  function valuetext(value) {
    return `${value}`;
  }

  return (
    <div>
      <Typography
        style={{
          fontSize: 25,
          backgroundColor: "#F9D576",
          padding: 10,
          color: orange[900],
        }}
        align="center"
      >
        EULER / EULER MEJORADO / RUNGE - KUTTE
      </Typography>
      <Grid container style={{ backgroundColor: "#FEEBB9", paddingBottom: 80 }}>
        {graficarFuncion && (
          <Grid item xs={8}>
            <FlexibleXYPlot
              width={900}
              height={650}
              style={{
                backgroundColor: "#FFFFFF",
                position: "center",
                margin: 20,
              }}
              dontCheckIfEmpty={true}
            >
              <VerticalGridLines />
              <HorizontalGridLines />
              <XAxis />
              <YAxis />
              <LineMarkSeries
                className="linemark-series-example"
                style={{
                  strokeWidth: "3px",
                }}
                lineStyle={{ stroke: "red" }}
                markStyle={{ stroke: "blue" }}
                data={funcionEuler(funcion, t0, tf, x0, n)}
                animation={true}
                onNearestXY={(valor) => setValor(valor)}
                size={2}
              />
              <LineMarkSeries
                className="linemark-series-example"
                style={{
                  strokeWidth: "3px",
                }}
                lineStyle={{ stroke: "green" }}
                markStyle={{ stroke: "blue" }}
                data={funcionEulerMejorado(funcion, t0, tf, x0, n)}
                animation={true}
                onNearestXY={(valor) => setValor(valor)}
                size={2}
              />
              <LineMarkSeries
                className="linemark-series-example"
                style={{
                  strokeWidth: "3px",
                }}
                lineStyle={{ stroke: "black" }}
                markStyle={{ stroke: "blue" }}
                data={funcionRungeKutta(funcion, t0, tf, x0, n)}
                animation={true}
                onNearestXY={(valor) => setValor(valor)}
                size={2}
              />

              {valor ? (
                <Hint
                  value={valor}
                  style={{
                    fontSize: 30,
                    text: {
                      display: "none",
                    },
                    value: {
                      color: "white",
                    },
                  }}
                />
              ) : null}
            </FlexibleXYPlot>
          </Grid>
        )}
        <Grid className={classes.paper} item xs={4}>
          <TextField
            className={classes.textFieldTop}
            // value={funcion}
            style={{ width: 250 }}
            variant="outlined"
            margin="dense"
            required
            fullWidth
            label="Función"
            onBlur={() => {
              //   setCambiandoProps(false);
            }}
            onChange={(e) => {
              setCambiandoProps(true);
              console.log(e.target.value);
              if (e.target.value.length === 0) {
                setFuncion("x + y");
              } else if (
                e.target.value.charAt(e.target.value.length - 1) === "+" ||
                e.target.value.charAt(e.target.value.length - 1) === "-" ||
                e.target.value.charAt(e.target.value.length - 1) === "/" ||
                e.target.value.charAt(e.target.value.length - 1) === "*"
              ) {
                console.log(e.target.value.slice(0, e.target.value.length - 1));
                setFuncion(e.target.value.slice(0, e.target.value.length - 1));
              } else setFuncion(e.target.value);
            }}
          />

          <TextField
            className={classes.textField}
            style={{ width: 250 }}
            variant="outlined"
            margin="dense"
            required
            fullWidth
            label="X(0)"
            /*  onBlur={() => {
                setCambiandoProps(false);
              }} */
            onChange={(e) => {
              setCambiandoProps(true);
              setX0(parseFloat(e.target.value));
            }}
          />
          <TextField
            className={classes.textField}
            style={{ width: 250 }}
            variant="outlined"
            margin="dense"
            required
            fullWidth
            label="t0"
            onChange={(e) => {
              setCambiandoProps(true);
              setT0(parseFloat(e.target.value));
            }}
          />
          <TextField
            className={classes.textField}
            style={{ width: 250 }}
            variant="outlined"
            margin="dense"
            required
            fullWidth
            onChange={(e) => {
              setCambiandoProps(true);
              setTF(parseFloat(e.target.value));
            }}
          />
          <TextField
            className={classes.textField}
            value={n}
            style={{ width: 250 }}
            variant="outlined"
            margin="dense"
            required
            fullWidth
            label="N"
            onChange={(e) => {
              setCambiandoProps(true);
              setN(parseFloat(e.target.value));
            }}
          />
          {/*   <Slider
            defaultValue={5}
            value={parseInt(n)}
            getAriaValueText={valuetext}
            aria-labelledby="discrete-slider-small-steps"
            step={1}
            marks
            min={1}
            max={20}
            valueLabelDisplay="auto"
            style={{ width: 250, marginTop: 20 }}
            onChange={(e, val) => setN(val)}
          /> */}
          <Button onClick={setGraficarFuncion(true)}>Graficar</Button>

          <Grid container>
            <Grid>
              <RemoveIcon
                style={{
                  fontSize: 50,
                  marginLeft: 20,
                  marginTop: 50,
                  color: red[500],
                }}
              />
            </Grid>
            <Grid>
              <Typography
                style={{ fontSize: 20, marginTop: 60, color: red[500] }}
              >
                {" "}
                Euler{" "}
              </Typography>
            </Grid>
          </Grid>
          <Grid container>
            <Grid>
              <RemoveIcon
                style={{ fontSize: 50, marginLeft: 20, color: green[700] }}
              />
            </Grid>
            <Grid>
              <Typography
                style={{ fontSize: 20, marginTop: 10, color: green[700] }}
              >
                {" "}
                Euler Mejorado{" "}
              </Typography>
            </Grid>
          </Grid>
          <Grid container>
            <Grid>
              <RemoveIcon style={{ fontSize: 50, marginLeft: 20 }} />
            </Grid>
            <Grid>
              <Typography style={{ fontSize: 20, marginTop: 10 }}>
                {" "}
                R-K{" "}
              </Typography>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </div>
  );
}

export default Prueba;

const useStyles = makeStyles((theme) => ({
  paper: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    boxShadow: "none",
    backgroundColor: "transparent",
  },

  textField: {
    background: "white",
    width: 250,
    marginTop: 30,
  },

  textFieldTop: {
    background: "white",
    width: 250,
    marginTop: 80,
  },

  button: {
    marginTop: 20,
    marginLeft: 20,
  },

  fondo: {
    color: "#FEECBE",
  },
}));
