import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import reportWebVitals from "./reportWebVitals";
import Prueba from "./Prueba";
import Montecarlo from "./Montecarlo";
import BackUp from "./Backup";

ReactDOM.render(
  <React.StrictMode>
    <BackUp />
    <Montecarlo />
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
